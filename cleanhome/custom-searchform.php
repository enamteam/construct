<form method="get" id="advanced-searchform" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-option">
        <input type="text" placeholder="<?php echo esc_attr__('Search...','clean-home' ); ?>" name="s">
        <button class="button" type="submit"><i class="fa fa-search"></i></button>
    </div>
    <a class="main-search" href="javascript:void(0)"><i class="fa fa-search"></i></a>
</form>