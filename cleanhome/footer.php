<?php global $clean_home_options; ?>
<footer>
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <?php
						if ( is_active_sidebar( 'footer_column_first' ) ) {
							dynamic_sidebar( 'footer_column_first' );
						}
					?>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 hidden-sm col-xs-12">
                    <?php
						if ( is_active_sidebar( 'footer_column_second' ) ) {
							dynamic_sidebar( 'footer_column_second' );
						}
					?>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <?php
						if ( is_active_sidebar( 'footer_column_third' ) ) {
							dynamic_sidebar( 'footer_column_third' );
						}
					?>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <?php
						if ( is_active_sidebar( 'footer_column_forth' ) ) {
							dynamic_sidebar( 'footer_column_forth' );
						}
					?>
                </div>
                <!-- end single footer -->
            </div>
        </div>
    </div>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="copyright">
                        <p>
                       <?php echo $clean_home_options['clean_home_footer_copywrite_text']; ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="copyright">
                    <?php
						if ( has_nav_menu( 'footer' ) ) {
							wp_nav_menu(
							  array(
								'theme_location' => 'footer',
								'container' => 'ul',
								'menu_class'=> ''
							  )
							);
							
                            } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?> 
  </body>
</html>