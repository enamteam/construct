<?php
require_once (dirname(__FILE__) . '/inc/redux/redux-config.php');
require_once (dirname(__FILE__) . '/inc/tgm/tgm-plugin-activation.php');
require_once (dirname(__FILE__) . '/inc/tgm/tgm-config.php');
require_once (dirname(__FILE__) . '/cleanhome-widgets/about-us-widget.php');
require_once (dirname(__FILE__) . '/cleanhome-widgets/info-widget.php');
require_once (dirname(__FILE__) . '/cleanhome-widgets/newsletter-widget.php');
require_once (dirname(__FILE__) . '/cleanhome-widgets/recent-post.php');

function cleanhome_enqueue_scripts() {
   
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '3.3.5 ' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7.0 ' );
	wp_enqueue_style( 'owl.transitions', get_template_directory_uri() . '/assets/css/owl.transitions.css', array(), '4.7.0 ' );
	wp_enqueue_style( 'owl.carousel', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), '4.7.0 ' );
	wp_enqueue_style( 'meanmenu', get_template_directory_uri() . '/assets/css/meanmenu.min.css', array(), '4.7.0 ' );
	
	wp_enqueue_style( 'icon', get_template_directory_uri() . '/assets/css/icon.css', array(), '4.7.0 ' );
	wp_enqueue_style( 'flaticon', get_template_directory_uri() . '/assets/css/flaticon.css', array(), '4.7.0 ' );
	wp_enqueue_style( 'magnific', get_template_directory_uri() . '/assets/css/magnific.min.css', array(), '4.7.0 ' );
	wp_enqueue_style( 'venobox', get_template_directory_uri() . '/assets/css/venobox.css' );
	wp_enqueue_style( 'cleanhome_custom', get_template_directory_uri() . '/assets/css/cleanhome.css' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/assets/css/responsive.css', array(), '4.7.0 ' );
	
	
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', array( 'jquery' ), '3.3.5 ', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '3.3.5 ', true );
    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '3.3.5 ', true );
    
    wp_enqueue_script( 'jquery_counterup', get_template_directory_uri() . '/assets/js/jquery.counterup.min.js', array( 'jquery' ), '3.3.5 ', true );
    wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/assets/js/waypoints.js', array( 'jquery' ), '3.3.5 ', true );
    wp_enqueue_script( 'isotope_pkgd', get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array( 'jquery' ), '3.3.5 ', true );
    wp_enqueue_script( 'jquery_stellar', get_template_directory_uri() . '/assets/js/jquery.stellar.min.js', array( 'jquery' ), '3.3.5 ', true );
    wp_enqueue_script( 'magnific', get_template_directory_uri() . '/assets/js/magnific.min.js', array( 'jquery' ), '3.3.5 ', true );
    wp_enqueue_script( 'venobox', get_template_directory_uri() . '/assets/js/venobox.min.js', array( 'jquery' ), '3.3.5 ', true );
    wp_enqueue_script( 'jquery_meanmenu', get_template_directory_uri() . '/assets/js/jquery.meanmenu.js', array( 'jquery' ), '3.3.5 ', true );
	wp_enqueue_script( 'form_validator', get_template_directory_uri() . '/assets/js/form-validator.min.js', array( 'jquery' ), '3.3.5 ', true );
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/assets/js/plugins.js"', array( 'jquery' ), '3.3.5 ', true );
	wp_enqueue_script( 'cleanhome_main', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '3.3.5 ', true );

	require_once( get_template_directory().'/assets/css/given-style.php');
	if (function_exists( 'cleanhome_inline_style' )) {
		$cleanhome_inline_style = cleanhome_inline_style();
		wp_add_inline_style( 'cleanhome_custom', $cleanhome_inline_style );
	}
}


add_action( 'wp_enqueue_scripts', 'cleanhome_enqueue_scripts' );
if ( ! function_exists( 'cleanhome_add_google_fonts' ) ) :

function cleanhome_add_google_fonts (){
	wp_enqueue_style( 'cleanhome-google-fonts', 'http://fonts.googleapis.com/css?family=Lato:400,400i,700,900|Montserrat:400,500,600,700,800,900', false ); 
}

endif;
 
add_action( 'wp_enqueue_scripts', 'cleanhome_add_google_fonts' );


if ( ! function_exists( 'cleanhome_theme_support_setup' ) ) :

 
function cleanhome_theme_support_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/custom_new
	 * If you're building a theme based on custom_new, use a find and replace
	 * to change 'custom_new' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'clean-home' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	
	$headerimage = array(
    'default-image' => '%s/images/logo.png',
    'width' => 1920,
    'height' => 1080,
    'flex-height' => false,
    'flex-width' => true,
    'uploads' => true,
    'random-default' => false,
    'header-text' => true,
    'default-text-color' => '',
    'wp-head-callback' => '',
    'admin-head-callback' => '',
    'admin-preview-callback' => '',
);
add_theme_support('custom-header', $headerimage);

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'clean-home' ),
		'top'  => __( 'Top', 'clean-home' ),
		'footer'  => __( 'Footer', 'clean-home' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	/*
	 * Enable support for custom logo.
	 *
	 * @since Twenty Fifteen 1.5
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
	) );


	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // custom_new_setup
add_action( 'after_setup_theme', 'cleanhome_theme_support_setup' );

if ( ! function_exists( 'cleanhome_widgets_init' ) ) :
function cleanhome_widgets_init() {
	register_sidebar(array(
        'name' => esc_html__('cleanhome Footer Column 1', 'clean-home'),
        'id' => 'footer_column_first',
        'description' => esc_html__('cleanhome Footer Column 1', 'clean-home'),
        'before_widget' => '<div id="%1$s" class="footer-content %2$s"><div class="footer-head">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="footer-1-wid-title footer-wid-title">',
        'after_title' => '</h4><hr>',
    ));
	register_sidebar(array(
        'name' => esc_html__('cleanhome Footer Column 2', 'clean-home'),
        'id' => 'footer_column_second',
        'description' => esc_html__('cleanhome Footer Column 2', 'clean-home'),
        'before_widget' => '<div id="%1$s" class="footer-content %2$s"><div class="footer-head">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="footer-2-wid-title footer-wid-title">',
        'after_title' => '</h4><hr>',
    ));
	register_sidebar(array(
        'name' => esc_html__('cleanhome Footer Column 3', 'clean-home'),
        'id' => 'footer_column_third',
        'description' => esc_html__('cleanhome Footer Column 3', 'clean-home'),
        'before_widget' => '<div id="%1$s" class="footer-content %2$s"><div class="footer-head">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="footer-3-wid-title footer-wid-title">',
        'after_title' => '</h4><hr>',
    ));
	register_sidebar(array(
        'name' => esc_html__('cleanhome Footer Column 4', 'clean-home'),
        'id' => 'footer_column_forth',
        'description' => esc_html__('cleanhome Footer Column 4', 'clean-home'),
        'before_widget' => '<div id="%1$s" class="footer-content %2$s"><div class="footer-head">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4 class="footer-4-wid-title footer-wid-title">',
        'after_title' => '</h4><hr>',
    ));
}




endif;




add_action('widgets_init', 'cleanhome_widgets_init');
function cleanhome_get_breadcrumb() {
	global $crypto_coin_options;
	if ( !is_home() && ! is_front_page() ){
	?>
	<div class="page-area">
            <div class="header-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb text-center">
                            <ul>
                                <li class="home-bread">Home</li>
                                <li>
                                <?php
    if (is_category() || is_single()) {
      
        the_category(' &bull; ');
            if (is_single()) {
               
                the_title();
            }
    } elseif (is_page()) {
       
        echo the_title();
    } elseif (is_search()) {
        echo the_search_query();
    }
?>
</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
	}
}	


