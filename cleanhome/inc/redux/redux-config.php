<?php

	if ( ! class_exists( 'Redux' ) ) {
		return;
	}

	// This is your option name where all the Redux data is stored.
    $opt_name = "clean_home_options";
	$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );
    $theme = wp_get_theme(); // For use with some settings. Not necessary.
    $args = array(
		// TYPICAL -> Change these values as you need/desire
		'opt_name'             => $opt_name,
		// This is where your data is stored in the database and also becomes your global variable name.
		'display_name'         => $theme->get( 'Name' ),
		// Name that appears at the top of your panel
		'display_version'      => $theme->get( 'Version' ),
		// Version that appears at the top of your panel
		'menu_type'            => 'menu',
		//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
		'allow_sub_menu'       => true,
		// Show the sections below the admin menu item or not
		'menu_title'           => __('Clean Home Options', 'clean-home'),
		'page_title'           => __('Clean Home Options', 'clean-home'),
		'google_api_key'       => '',
		'google_update_weekly' => false,
		'disable_google_fonts_link' => true,
		'async_typography'     => false,
		'admin_bar'            => true,
		'admin_bar_icon'       => 'dashicons-portfolio',
		// Choose an icon for the admin bar menu
		'admin_bar_priority'   => 50,
		// Choose an priority for the admin bar menu
		'global_variable'      => '',
		// Set a different name for your global variable other than the opt_name
		'dev_mode'             => false,
		// Show the time the page took to load, etc
		'update_notice'        => true,
		// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
		'customizer'           => false,
		// OPTIONAL -> Give you extra features
		'page_priority'        => null,
		// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
		'page_parent'          => 'themes.php',
		// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
		'page_permissions'     => 'manage_options',
		// Permissions needed to access the options panel.
		'menu_icon'            => '',
		// Specify a custom URL to an icon
		'last_tab'             => '',
		// Force your panel to always open to a specific tab (by id)
		'page_icon'            => 'icon-themes',
		// Icon displayed in the admin panel next to your menu_title
		'page_slug'            => '',
		// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
		'save_defaults'        => true,
		// On load save the defaults to DB before user clicks save or not
		'default_show'         => false,
		// If true, shows the default value next to each field that is not the default value.
		'default_mark'         => '',
		// What to print by the field's title if the value shown is default. Suggested: *
		'show_import_export'   => true,
		// Shows the Import/Export panel when not used as a field.
		// CAREFUL -> These options are for advanced use only
		'transient_time'       => 60 * MINUTE_IN_SECONDS,
		'output'               => true,
		// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
		'output_tag'           => true,
		// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
		// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
		'database'             => '',
		// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
		'use_cdn'              => true,
		// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.
		// HINTS
		'hints'                => array(
			'icon'          => 'el el-question-sign',
			'icon_position' => 'right',
			'icon_color'    => 'lightgray',
			'icon_size'     => 'normal',
			'tip_style'     => array(
				'color'   => 'red',
				'shadow'  => true,
				'rounded' => false,
				'style'   => '',
			),
			'tip_position'  => array(
				'my' => 'top left',
				'at' => 'bottom right',
			),
			'tip_effect'    => array(
				'show' => array(
					'effect'   => 'slide',
					'duration' => '500',
					'event'    => 'mouseover',
				),
				'hide' => array(
					'effect'   => 'slide',
					'duration' => '500',
					'event'    => 'click mouseleave',
				),
			),
		)
	);


	Redux::setArgs( $opt_name, $args );

	Redux::setSection( $opt_name, array(
		'title'            => __('Header Settings', 'clean-home'),
		'id'               => 'header_settings',
		'desc'             => __('All header settings', 'clean-home'),
		'customizer_width' => '400px',
		'icon'             => 'el el-website',
		'fields'           => array(
		
		
			
			array(
                'id'       => 'clean_home_is_sticky_header',
                'type'     => 'switch',
                'title'    => __('Active Header Sticky', 'clean-home'),
                'subtitle' => __('Enable or Disable Sticky Header', 'clean-home'),
                'default'  => 'off',
                'on'       => __('Enable', 'clean-home'),
                'off'      => __('Disable', 'clean-home'),
            ),
			array(
                'id'       => 'clean_home_is_preloader',
                'type'     => 'switch',
                'title'    => __('Active Page Loader', 'clean-home'),
                'subtitle' => __('Enable or Disable Page Loader', 'clean-home'),
                'default'  => 'off',
                'on'       => __('Enable', 'clean-home'),
                'off'      => __('Disable', 'clean-home'),
            ),
			array(
                'id'        => 'clean_home_preloader_brackground',
                'type'      => 'media',
                'url'       => true,
                'title'     => __('Preloader Image', 'clean-home'),
                'compiler'  => 'true',
                'desc'      => __('Set Preloader Brackground', 'clean-home'),
                'subtitle'  => __('Upload A Image Preloader Brackground', 'clean-home'),
                'default'   => array('url' => get_template_directory_uri().'/assets/img/logo/preloader.gif'),
				
            ),
			array(
				'id'        => 'clean_home_color_preloder_background',
				'type'      => 'color_rgba',
				'title'     => 'Header Color Picker',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'output'	=> array('background-color' => '#preloader'),
				
				'default'   => array(
					'color'     => '#ffffff',
					'alpha'     => 1
				),
				'options'       => array(
					'show_input'                => true,
					'show_initial'              => true,
					'show_alpha'                => true,
					'show_palette'              => true,
					'show_palette_only'         => false,
					'show_selection_palette'    => true,
					'max_palette_size'          => 10,
					'allow_empty'               => true,
					'clickout_fires_change'     => false,
					'choose_text'               => 'Choose',
					'cancel_text'               => 'Cancel',
					'show_buttons'              => true,
					'use_extended_classes'      => true,
					'palette'                   => null,  // show default
					'input_text'                => 'Select Color'
				),                        
			),
			array(
				'id'       => 'clean_home_logo',
				'type'     => 'media',
				'url'       => true,
				'compiler'  => 'true',
                'desc'      => __('Logo uploder.', 'clean-home'),
				'subtitle'  => __('Upload logo using the WordPress uploader', 'clean-home'),
				'title'    => __('Site Logo', 'clean-home'),
				'default'     => array(
					'url'       => get_template_directory_uri() . '/images/logo/logo.png',
				),
			),

		
			
            array(
                'id'        => 'clean_home_site_favicon',
                'type'      => 'media',
                'url'       => true,
                'title'     => __('Favicon URL', 'clean-home'),
                'compiler'  => 'true',
                'desc'      => __('Set favicon', 'clean-home'),
                'subtitle'  => __('Upload a favicon for the theme', 'clean-home'),
                'default'   => array('url' => get_template_directory_uri().'/images/logo/favicon.ico'),
            ),
            array(
				'id'       => 'clean_home_header_mail_switch',
				'type'     => 'switch',
				'title'    => __('Display Header Location', 'clean-home'),
				'default'  => 'off',
				'on'	   => __('Enable', 'clean-home'),
				'off'	   => __('Disable', 'clean-home'),
			),
            array(
                'id'       => 'clean_home_header_mail',
                'type'     => 'editor',
				'title'    => __('Header Location', 'clean-home'), 
				'default'    => 'House-34,22/2 avenue'
            ),
			array(
                'id'       => 'clean_home_header_mail_second',
                'type'     => 'editor',
				'title'    => __('Header Location Second Line', 'clean-home'), 
				'default'    => 'New york, United States'
            ),
			array(
                'id'       => 'clean_home_header_mail_icon',
                'type'     => 'text',
				'title'    => __('Header Location Icon Class', 'clean-home'), 
				'default'    => 'fa fa-map-marker'
            ),
			array(
				'id'       => 'clean_home_header_phone_switch',
				'type'     => 'switch',
				'title'    => __('Display Header phone number', 'clean-home'),
				'default'  => 'off',
				'on'	   => __('Enable', 'clean-home'),
				'off'	   => __('Disable', 'clean-home'),
			),
			array(
                'id'       => 'clean_home_header_phone',
                'type'     => 'editor',
				'title'    => __('Header Phone address', 'clean-home'), 
				'default'    => '+1313-4535434'
            ),
			array(
                'id'       => 'clean_home_header_phone_second',
                'type'     => 'editor',
				'title'    => __('Header Phone Second Line', 'clean-home'), 
				'default'    => 'info@cleanservices.com'
            ),
			array(
                'id'       => 'clean_home_header_phone_icon',
                'type'     => 'text',
				'title'    => __('Header Phone Icon', 'clean-home'), 
				'default'    => 'fa fa-phone'
            ),
			
			array(
				'id'       => 'clean_home_header_button_switch',
				'type'     => 'switch',
				'title'    => __('Display Header Button', 'clean-home'),
				'default'  => 'off',
				'on'	   => __('Enable', 'clean-home'),
				'off'	   => __('Disable', 'clean-home'),
			),
			
			array(
                'id'       => 'clean_home_header_button_title',
                'type'     => 'text',
				'title'    => __('Header Button Title', 'clean-home'), 
				'default'    => 'Get a Qaote'
            ),
			array(
                'id'       => 'clean_home_header_button_modal',
                'type'     => 'editor',
				'title'    => __('Header Modal form', 'clean-home'), 
				'default'    => ''
            ),
			
			array(
				'id'       => 'clean_home_header_top_switch',
				'type'     => 'switch',
				'title'    => __('Display Header Top Location', 'clean-home'),
				'default'  => 'off',
				'on'	   => __('Enable', 'clean-home'),
				'off'	   => __('Disable', 'clean-home'),
			),
			array(
                'id'       => 'clean_home_header_top_text',
                'type'     => 'editor',
				'title'    => __('Header Top Welcome Text', 'clean-home'), 
				'default'    => 'Welcome Our Cleaning Services'
            ),
			array(
				'id'       => 'clean_home_fb_link',
				'type'     => 'text',
				'title'    => __('Facebook Link', 'clean-home'),
				'default'  => '#'
			),
			array(
				'id'       => 'clean_home_tw_link',
				'type'     => 'text',
				'title'    => __('Twitter Link', 'clean-home'),
				'default'  => '#'
			),
			array(
				'id'       => 'clean_home_gp_link',
				'type'     => 'text',
				'title'    => __('Google Plus Link', 'clean-home'),
				'default'  => '#'
			),
			array(
				'id'       => 'clean_home_pin_link',
				'type'     => 'text',
				'title'    => __('Pinterest Link', 'clean-home'),
				'default'  => '#'
			),
			array(
				'id'       => 'clean_home_skype_link',
				'type'     => 'text',
				'title'    => __('Skype', 'clean-home'),
				'default'  => '#'
			),
			
			
			array(
				'id'        => 'clean_home_color_rgba_header_top',
				'type'      => 'color_rgba',
				'title'     => 'Header top Color Picker',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'output'	=> array('background-color' => '.topbar-area'),
				'default'   => array(
					'color'     => '#f9f9f9',
					'alpha'     => 1
				),
				'options'       => array(
					'show_input'                => true,
					'show_initial'              => true,
					'show_alpha'                => true,
					'show_palette'              => true,
					'show_palette_only'         => false,
					'show_selection_palette'    => true,
					'max_palette_size'          => 10,
					'allow_empty'               => true,
					'clickout_fires_change'     => false,
					'choose_text'               => 'Choose',
					'cancel_text'               => 'Cancel',
					'show_buttons'              => true,
					'use_extended_classes'      => true,
					'palette'                   => null,  // show default
					'input_text'                => 'Select Color'
				),                        
			),
			array(
				'id'        => 'clean_home_color_rgba_header',
				'type'      => 'color_rgba',
				'title'     => 'Header Color Picker',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'output'	=> array('background-color' => '.header-middle-area'),
				//'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
				'default'   => array(
					'color'     => '#63c672',
					'alpha'     => 1
				),
				'options'       => array(
					'show_input'                => true,
					'show_initial'              => true,
					'show_alpha'                => true,
					'show_palette'              => true,
					'show_palette_only'         => false,
					'show_selection_palette'    => true,
					'max_palette_size'          => 10,
					'allow_empty'               => true,
					'clickout_fires_change'     => false,
					'choose_text'               => 'Choose',
					'cancel_text'               => 'Cancel',
					'show_buttons'              => true,
					'use_extended_classes'      => true,
					'palette'                   => null,  // show default
					'input_text'                => 'Select Color'
				),                        
			),
			
			array(
				'id'        => 'clean_home_color_rgba_header_menu',
				'type'      => 'color_rgba',
				'title'     => 'Header Menu Color Picker',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'output'	=> array('background-color' => '#sticker'),
				//'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
				'default'   => array(
					'color'     => '#63c672',
					'alpha'     => 1
				),
				'options'       => array(
					'show_input'                => true,
					'show_initial'              => true,
					'show_alpha'                => true,
					'show_palette'              => true,
					'show_palette_only'         => false,
					'show_selection_palette'    => true,
					'max_palette_size'          => 10,
					'allow_empty'               => true,
					'clickout_fires_change'     => false,
					'choose_text'               => 'Choose',
					'cancel_text'               => 'Cancel',
					'show_buttons'              => true,
					'use_extended_classes'      => true,
					'palette'                   => null,  // show default
					'input_text'                => 'Select Color'
				),                        
			),
			
			
		)
	) );
    

    Redux::setSection( $opt_name, array(
		'title'            => __('Breadcrumb', 'clean-home'),
		'id'               => 'breadcrumb_settings',
		'desc'             => __('Breadcrumb', 'clean-home'),
		'customizer_width' => '400px',
		'icon'             => 'el el-font',
		'fields'           => array(

			array(
				'id'       => 'clean_home_breadcrumb_settings',
				'type'     => 'switch',
				'title'    => __('Display Header phone number', 'clean-home'),
				'default'  => true,
				'on'	   => __('Enable', 'clean-home'),
				'off'	   => __('Disable', 'clean-home'),
			),
			 array(
                'id'        => 'clean_home_breadcrumb_brackground',
                'type'      => 'media',
                'url'       => true,
                'title'     => __('Breadcrumb Brackground', 'clean-home'),
                'compiler'  => 'true',
                'desc'      => __('Set Breadcrumb Brackground', 'clean-home'),
                'subtitle'  => __('Upload A Image Breadcrumb Brackground', 'clean-home'),
                'default'   => array('url' => get_template_directory_uri().'/images/background/b.jpg'),
				
            ),

	)));

	Redux::setSection( $opt_name, array(
		'title'            => __('Footer', 'clean-home'),
		'id'               => 'footer_settings',
		'desc'             => __('Footer', 'clean-home'),
		'customizer_width' => '400px',
		'icon'             => 'el el-font',
		'fields'           => array(
			array(
				'id'       => 'clean_home_footer_copywrite_text',
				'type'     => 'text',
				'title'    => __('Footer copywrite text', 'clean-home'),
				'default'  => 'Copyright &copy; 2018 <a href="#">Cleanhome</a> All Rights Reserved'
			),
			array(
				'id'        => 'clean_home_color_rgba_footer',
				'type'      => 'color_rgba',
				'title'     => 'Footer Color Picker',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'output'	=> array('background-color' => '.footer-area'),
				'default'   => array(
					'color'     => '#252525',
					'alpha'     => 1
				),
				'options'       => array(
					'show_input'                => true,
					'show_initial'              => true,
					'show_alpha'                => true,
					'show_palette'              => true,
					'show_palette_only'         => false,
					'show_selection_palette'    => true,
					'max_palette_size'          => 10,
					'allow_empty'               => true,
					'clickout_fires_change'     => false,
					'choose_text'               => 'Choose',
					'cancel_text'               => 'Cancel',
					'show_buttons'              => true,
					'use_extended_classes'      => true,
					'palette'                   => null,  // show default
					'input_text'                => 'Select Color'
				),                        
			),
			array(
				'id'        => 'clean_home_color_rgba_footer_bottom',
				'type'      => 'color_rgba',
				'title'     => 'Footer Bottom Color Picker',
				'subtitle'  => 'Set color and alpha channel',
				'desc'      => 'The caption of this button may be changed to whatever you like!',
				'output'	=> array('background-color' => '.footer-area-bottom'),
				//'compiler'  => array('color' => '.site-header, .site-footer', 'background-color' => '.nav-bar'),
				'default'   => array(
					'color'     => '#1d1d1f',
					'alpha'     => 1
				),
				'options'       => array(
					'show_input'                => true,
					'show_initial'              => true,
					'show_alpha'                => true,
					'show_palette'              => true,
					'show_palette_only'         => false,
					'show_selection_palette'    => true,
					'max_palette_size'          => 10,
					'allow_empty'               => true,
					'clickout_fires_change'     => false,
					'choose_text'               => 'Choose',
					'cancel_text'               => 'Cancel',
					'show_buttons'              => true,
					'use_extended_classes'      => true,
					'palette'                   => null,  // show default
					'input_text'                => 'Select Color'
				),                        
			),
			
			


	)));

    Redux::setSection( $opt_name, array() );

    Redux::setSection( $opt_name, array());