<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package crypto-coin
 */

?>

<div  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- Block -->
			<div class="offset-sm">
			<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'crypto-coin' ),
				'after'  => '</div>',
			) );
		?>
			</div>
			<!-- //Block -->
		</div>