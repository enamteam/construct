<?php

// Register Custom Post Type
function construct_additional_coupons_post_type() {

    $labels = array(
        'name' => _x('Project', 'Post Type General Name', 'construct-additional'),
        'singular_name' => _x('Project', 'Post Type Singular Name', 'construct-additional'),
        'menu_name' => __('Projects', 'construct-additional'),
        'name_admin_bar' => __('Project', 'construct-additional'),
        'archives' => __('Item Archives', 'construct-additional'),
        'parent_item_colon' => __('Parent Item:', 'construct-additional'),
        'all_items' => __('All Projects', 'construct-additional'),
        'add_new_item' => __('Add New Project', 'construct-additional'),
        'add_new' => __('Add New Project', 'construct-additional'),
        'new_item' => __('New Service Item', 'construct-additional'),
        'edit_item' => __('Edit Project Item', 'construct-additional'),
        'update_item' => __('Update Project Item', 'construct-additional'),
        'view_item' => __('View Project Item', 'construct-additional'),
        'search_items' => __('Search Item', 'construct-additional'),
        'not_found' => __('Not found', 'construct-additional'),
        'not_found_in_trash' => __('Not found in Trash', 'construct-additional'),
        'featured_image' => __('Featured Image', 'construct-additional'),
        'set_featured_image' => __('Set featured image', 'construct-additional'),
        'remove_featured_image' => __('Remove featured image', 'construct-additional'),
        'use_featured_image' => __('Use as featured image', 'construct-additional'),
        'insert_into_item' => __('Insert into item', 'construct-additional'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'construct-additional'),
        'items_list' => __('Items list', 'construct-additional'),
        'items_list_navigation' => __('Items list navigation', 'construct-additional'),
        'filter_items_list' => __('Filter items list', 'construct-additional'),
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'construct-additional'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'project'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'thumbnail','editor'),
    );

    register_post_type('project', $args);
}

add_action('init', 'construct_additional_coupons_post_type');
