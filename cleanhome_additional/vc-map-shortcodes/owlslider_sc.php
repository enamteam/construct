<?php
 class cryptocoin_OwlSlider  extends WPBakeryShortCode{
   
    public function __construct()
    {
        add_shortcode( 'cryptocoin_owlslider', array($this, 'cryptocoin_owlslider_method'));
        add_shortcode( 'cryptocoin_owlslider_content', array($this, 'cryptocoin_owlslider_content_method'));
    }

 
    
   public function cryptocoin_owlslider_method ($atts, $content = null){
        extract(shortcode_atts(array(
            'navigation_type' => 0,
            'extra_class' => '',
        ), $atts));
            ob_start();
            ?>
            
        <!-- Slider -->
        
             <div class="intro-area intro-area-2 <?php if( $extra_class != '' ){ echo esc_attr($extra_class); } ?>">
           <div class="main-overly"></div>
            <div class="intro-carousel">
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>  
    
          <?php
        $output = ob_get_clean();
      return $output;
    }

     

   public function cryptocoin_owlslider_content_method ($atts, $content = null){
        extract(shortcode_atts(array(
            'image' => '',
            'title'=>'',
			'title1'=>'',
			'subtitle'=>'',
           
            'button_1_link' => '',
            'button_2_link' => '',
			'extraclass'=>''
        ), $atts ));
		
		$button_1_href = vc_build_link( $button_1_link ) ; 
		$button_2_href = vc_build_link( $button_2_link ) ; 
        $attachement = wp_get_attachment_image_src((int) $image, 'full');
        ob_start();
        ?>
        
        
        
        
        
        
        <div class="intro-content">
                    <div class="slider-images">
                        <img src="<?php echo esc_url( $attachement[0] ); ?>" alt="">
                    </div>
                    <div class="slider-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <!-- layer 1 -->
                                            <div class="layer-1">
                                                <h1 class="title2"><?php echo $title ?> <br/><span class="color"><?php echo $title1 ?></span></h1>
                                            </div>
                                            <!-- layer 2 -->
                                            <div class="layer-2 ">
                                                <p><?php echo $subtitle ?></p>
                                            </div>
                                            <!-- layer 3 -->
                                            <div class="layer-3">
                                             <a href="<?php echo $button_1_href['url'];?>" <?php if(!(empty($button_1_href['target']))):?> target="<?php echo $button_1_href['target'];?>" <?php endif;?>  class="ready-btn left-btn <?php echo esc_html($extra_class); ?>" rel="<?php echo $button_1_href['rel'];?>"  >   
                                                <?php echo $button_1_href['title'];?>
                                            </a>
                                             <a href="<?php echo $button_2_href['url'];?>" <?php if(!(empty($button_2_href['target']))):?> target="<?php echo $button_2_href['target'];?>" <?php endif;?>  class="ready-btn right-btn <?php echo esc_html($extra_class); ?>" rel="<?php echo $button_2_href['rel'];?>"  >   
                                                <?php echo $button_2_href['title'];?>
                                            </a>
                                            
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        

         
             
        <?php
        return ob_get_clean();
    }
}

    new cryptocoin_OwlSlider();