<?php

class electricianCoupns {

    public function __construct() {
        add_shortcode('electrician_coupns', array($this, 'electrician_coupns_func'));
    }

    public function electrician_coupns_func($atts, $content = null) {
        extract(shortcode_atts(array(
            'extra_class' => '',
            'column' => 2,
            'per_page' => -1,
                        ), $atts));


        $orderby = 'DESC';
        $args = array(
            'posts_per_page' => $per_page,
            'post_type' => 'project',
            'orderby' => $orderby,
            'no_found_rows' => true,
        );

        $column_no = $column;
        switch ((int) $column_no) {
            case 2:
                $colclass = 'col-sm-6 col-xs-12';
                break;
            case 4:
                $colclass = 'col-md-3 col-sm-4 col-xs-12';
                break;
            default:
                $colclass = 'col-md-4 col-sm-4 col-xs-12';
                break;
        }
        $query = new WP_Query($args);
        ob_start();
        ?>
        <?php
        if ($query->have_posts()) :
            ?>
            <?php
            while ($query->have_posts()) : $query->the_post();
                $post_id = get_the_ID();
            
                ?>

                <div class="col-lg-6<?php echo esc_html__($colclass, 'electrician');   ?>">
                    
                    
                    
                    
							<div class="well-services">
								<div class="services-img">
									<img alt="" src="<?php echo get_the_post_thumbnail_url() ?>">
								</div>
								<div class="main-services">
									<div class="service-content">
										<h4><?php echo get_the_title()  ?></h4>
										<p><?php echo wp_trim_words( get_the_content(), 20,'' ); ?></p>
										<a href="<?php the_permalink(); ?> " class="service-btn">read more</a>
									</div>
								</div>
							</div>
						
                    
                </div>
                <?php
            endwhile;
            ?>
        <?php endif; ?>   
        <?php
        $output = ob_get_clean();
        return $output;
    }

}

new electricianCoupns();
