<?php
 class cryptocoin_Welcome  extends WPBakeryShortCode{
   
    public function __construct()
    {
        add_shortcode( 'cryptocoin_welcome', array($this, 'cryptocoin_welcome_method'));
        add_shortcode( 'cryptocoin_welcome_content', array($this, 'cryptocoin_welcome_content_method'));
    }

 
    
   public function cryptocoin_welcome_method ($atts, $content = null){
        extract(shortcode_atts(array(
            'navigation_type' => 0,
            'extra_class' => '',
        ), $atts));
            ob_start();
            ?>
            
        <div class="container">
        <div class="row">
        <div class="wel-service <?php if( $extra_class != '' ){ echo esc_attr($extra_class); } ?>">
                    <?php echo do_shortcode($content); ?>
            </div>  
             </div>  
              </div>  
    
          <?php
        $output = ob_get_clean();
      return $output;
    }

     

   public function cryptocoin_welcome_content_method ($atts, $content = null){
        extract(shortcode_atts(array(
            'number' => '',
            'title'=>'',
			'icon_fontawesome'=>''
        ), $atts ));
		
        ob_start();
        ?>
<div class="col-md-4 col-sm-4 col-xs-12">
                            <div data-wow-delay=".1s" data-wow-duration="1s" class="wel-service-details text-center wow slideInUp" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: slideInUp;">
                                <div class="wel-single-service">
                                    <div class="wel-icon-box">
                                       <span><?php echo esc_html($number);?></span>
                                        <a href="#" class="wel-icon"><i class="<?php echo esc_html($icon_fontawesome);?>"></i></a>
                                    </div>
                                    <div class="wel-service-text">
                                        <h4><?php echo esc_html($title);?></h4>
                                        <p>
                                            <?php echo esc_html($content);?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

             
        <?php
        return ob_get_clean();
    }
}

    new cryptocoin_Welcome();