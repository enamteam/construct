<?php
vc_map(array(
    "name" => "Slick Slider",
    "base" => "cryptocoin_owlslider",
    "category" => 'cryptocoin',
    "as_parent" => array('only' => 'cryptocoin_owlslider_content'),
    "content_element" => true,
    
    "show_settings_on_create" => true,
    "js_view" => 'VcColumnView',
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => "Extra Class",
            "param_name" => "extra_class",
        ),
    )
));

vc_map(array(
    "name" => "Slick Slider Items",
    "base" => "cryptocoin_owlslider_content",
    "category" => 'cryptocoin',
    "as_child" => array('only' => 'cryptocoin_owlslider'),
    "content_element" => true,
    
    "show_settings_on_create" => true,
    "params" => array(
        array(
            "type" => "attach_image",
            "heading" => esc_html__("Slider Image", TEXT_DOMAIN),
            "param_name" => "image",
        ),
       
        array(
            "type" => "textfield",
            "heading" => "Title",
            "param_name" => "title",
            "admin_label" => false,
        ),
		array(
            "type" => "textfield",
            "heading" => "Title 1",
            "param_name" => "title1",
            "admin_label" => false,
        ),
		array(
            "type" => "textfield",
            "heading" => "Sub Title",
            "param_name" => "subtitle",
            "admin_label" => false,
        ),
		
		array(
                "type" => "vc_link",
                "holder" => "div",
                "heading" => "Button One Link",
                "param_name" => "button_1_link",
        ),
		array(
                "type" => "vc_link",
                "holder" => "div",
                "heading" => "Button Two Link",
                "param_name" => "button_2_link",
             ),
		 array(
            "type" => "textfield",
            "heading" => "Add Extra Class",
            "param_name" => "extraclass",
        ),
    )
));

if (class_exists('WPBakeryShortCodesContainer')) {

    class WPBakeryShortCode_cryptocoin_Owlslider extends WPBakeryShortCodesContainer {
        
    }

}
if (class_exists('WPBakeryShortCode')) {

    class WPBakeryShortCode_cryptocoin_Owlslider_Content extends WPBakeryShortCode {
        
    }

}

