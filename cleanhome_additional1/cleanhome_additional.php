<?php
/*
  Plugin Name: Cleanhome Additional
  Plugin URI: http://oheethemes.com/
  Description: Helping for the cryptocoin  theme.
  Author: oheethemes
  Version: 1.0
  Author URI: http://oheethemes.com/
 */
 
define('PLUGIN_DIR', dirname(__FILE__) . '/');
define('TEXT_DOMAIN', 'cleanhome-additional');

$classesDir = array(
    PLUGIN_DIR . 'vc-map-shortcodes/',
    PLUGIN_DIR . 'vc-map/'
);

function __autoloadShortCode() {
    global $classesDir;
    foreach ($classesDir as $directory) {
        foreach (glob($directory . '*.php') as $filename) {
            if (file_exists($filename)) {
                include_once ($filename);
            }
        }
    }
}
function __autoloadVcMap() {
    __autoloadShortCode();
}

add_action('vc_before_init', '__autoloadVcMap');
