<?php
class Cleanhome_Additional_imageanddetails_content  extends WPBakeryShortCode{
   
	public function __construct()
	{
		add_shortcode( 'Cleanhome_Additional_imageanddetails_content', array($this, 'Cleanhome_Additional_imageanddetails_content_function'));
	}


	public function Cleanhome_Additional_imageanddetails_content_function($atts, $content = null) {
	
		extract(shortcode_atts(array(
			'title' => '',
			'image' => '',
			'sub_title' => '',
			'btn_title' => '',
			'button_target' => '2',
			'modal_id' => 'appointmentForm',
			'popup_id' => '54',
			'extra_class' => '',
			'link'=> '',
						), $atts));
		$button_2_href = vc_build_link( $button_2_link ) ; 
		$attachement = wp_get_attachment_image_src((int) $image, 'full');
		ob_start();
		
	   ?>
	<div class="about-area area-padding">
				<div class="container">
					<div class="row">
						<!-- start about images -->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="about-images">
								<img src="<?php echo $attachement[0] ?>" alt="">
							</div>
						</div>
						<!-- start about text -->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="about-text">
								<h3 class="sec-head"><?php echo esc_html($title) ?></h3>
								<h4><?php echo esc_html($sub_title) ?></h4>
								<p>
								   <?php echo esc_html($content) ?>
								</p>
	<div class="about-button text-right">
	
								<?php
	
								if (button_target == '2'):
									?>
									<a class="service-btn <?php echo esc_html($extra_class); ?>" href="#" data-toggle="modal" data-target="#<?php echo esc_html($modal_id); ?>"><?php echo esc_html($btn_title); ?></a>
								<?php elseif(button_target == '3'): ?>
										<a class="service-btn <?php echo esc_html($extra_class); ?>" href="#"><?php echo esc_html($btn_title); ?></a>
										<div class="form-popup">
										   <?php echo do_shortcode('[contact-form-7 id="' . $popup_id . '"]'); ?>
										</div>
									
								<?php else: ?>
								   <?php  $href = vc_build_link( $link ) ;  ?>
													 <a href="<?php echo $href['url'];?>" <?php if(!(empty($href['target']))):?> target="<?php echo $href['target'];?>" <?php endif;?>  class="service-btn <?php echo esc_html($extra_class); ?>" rel="<?php echo $href['rel'];?>"  >   
										<?php echo esc_html($btn_title); ?>
									</a>
								<?php endif; ?>
	
	
	</div>
	
	
							</div>
						</div>
						<!-- End col-->
					</div>
				</div>
			</div>
	
	
		<?php
		$content = ob_get_clean();
		return $content;
	}
}

 new Cleanhome_Additional_imageanddetails_content();