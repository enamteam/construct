<?php
 class Cleanhome_Additional_OwlSlider  extends WPBakeryShortCode{
   
    public function __construct()
    {
        add_shortcode( 'Cleanhome_Additional_owlslider', array($this, 'Cleanhome_Additional_owlslider_method'));
        add_shortcode( 'Cleanhome_Additional_owlslider_content', array($this, 'Cleanhome_Additional_owlslider_content_method'));
    }

 
    
   public function Cleanhome_Additional_owlslider_method ($atts, $content = null){
        extract(shortcode_atts(array(
            'navigation_type' => 0,
            'extra_class' => '',
        ), $atts));
            ob_start();
            ?>
            
        <!-- Slider -->
        <div id="home" class="intro-area <?php if( $extra_class != '' ){ echo esc_attr($extra_class); } ?>">
            <div class="main-overly"></div>
            <div class="intro-carousel">
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>  
    
          <?php
        $output = ob_get_clean();
      return $output;
    }

     

   public function Cleanhome_Additional_owlslider_content_method ($atts, $content = null){
        extract(shortcode_atts(array(
            'image' => '',
            'title1'=>'',
			'title2'=>'',
			'subtitle'=>'',
            'button_1' => '',
            'button_2' => '',
            'button_1_link' => '',
            'button_2_link' => '',
			'extraclass'=>''
        ), $atts ));
		
		$button_1_href = vc_build_link( $button_1_link ) ; 
		$button_2_href = vc_build_link( $button_2_link ) ; 
        $attachement = wp_get_attachment_image_src((int) $image, 'full');
        ob_start();
        ?>

<div class="intro-content">
<div class="slider-images">
                        <img alt="" src="<?php echo esc_url( $attachement[0] ); ?>">
                    </div>
                    <div class="slider-content">
                        <div class="display-table">
                            <div class="display-table-cell">
                                <div class="container">
                                    <div class="row">
                                    
                                    
                                    
                                    
                                    <div class="col-md-12 text-center">
                                            <!-- layer 1 -->
                                            <div class="layer-1">
                                                <h1 class="title2"><?php echo $title1 ?><br> <?php echo $title2 ?></h1>
                                            </div>
                                            <!-- layer 2 -->
                                            <div class="layer-2 ">
                                                <p><?php echo $subtitle ?></p>
                                            </div>
                                            <!-- layer 3 -->
                                            <div class="layer-3">
                                                <a href="<?php echo $button_1_href['url'];?>" <?php if(!(empty($button_1_href['target']))):?> target="<?php echo $button_1_href['target'];?>" <?php endif;?>  class="ready-btn left-btn <?php echo esc_attr($extraclass); ?>" rel="<?php echo $button_1_href['rel'];?>"  >   
                                                <?php echo esc_html__($button_1_href['title']); ?>
                                            </a>
                                             <a href="<?php echo $button_2_href['url'];?>" <?php if(!(empty($button_2_href['target']))):?> target="<?php echo $button_2_href['target'];?>" <?php endif;?>  class="ready-btn right-btn <?php echo esc_attr($extraclass); ?>" rel="<?php echo $button_2_href['rel'];?>"  >   
                                                <?php echo  esc_html__($button_2_href['title']); ?>
                                            </a>
                                            </div>
                                        </div>
                                    
                                    
                                    
                                    
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             
        <?php
        return ob_get_clean();
    }
}

    new Cleanhome_Additional_OwlSlider();