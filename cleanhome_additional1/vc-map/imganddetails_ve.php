<?php
$c_f_seven = get_posts('post_type="wpcf7_contact_form"&numberposts=-1');

$contact_forms = array();
if ($c_f_seven) {
    foreach ($c_f_seven as $c_f_seven_form) {
        $contact_forms[$c_f_seven_form->post_title] = $c_f_seven_form->ID;
    }
} else {
    $contact_forms[__('No contact forms found', 'js_composer')] = 0;
}
vc_map(array(
    "name" => "Image And Details",
    "base" => "Cleanhome_Additional_imageanddetails_content",
    "category" => 'Theme Additional',
    "content_element" => true,
    
    "show_settings_on_create" => true,
    "params" => array(
        array(
            "type" => "attach_image",
            "heading" => esc_html__("Slider Image", TEXT_DOMAIN),
            "param_name" => "image",
        ),
       
        array(
            "type" => "textfield",
            "heading" => "Title",
            "param_name" => "title",
            "admin_label" => false,
        ),
        array(
            "type" => "textfield",
            "heading" => "Sub Title",
            "param_name" => "sub_title",
            "admin_label" => false,
        ),
		array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Description", TEXT_DOMAIN),
            "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
            "value" => __( "", TEXT_DOMAIN ),
            "description" => __( "Enter your Description.", "my-text-domain" )
         ),
         array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => "Button Title",
            "param_name" => "btn_title",
            "admin_label" => true,
        ),
         array(
            "type" => "textfield",
            "heading" => "Add class into button",
            "param_name" => "extra_class",
            "admin_label" => false,
        ),
		array(
            'type' => 'dropdown',
            'heading' => esc_html__('Button Action', TEXT_DOMAIN),
            'param_name' => 'button_target',
            'value' => array(
                'None' => '1',
                'Modal' => '2',
                'Pop Up' => '3',
                 'Link' => '4',
            )
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => "Modal Element Id",
            "param_name" => "modal_id",
            'dependency' => array(
                'element' => 'button_target',
                'value' => '2',
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Select contact form for Pop Up Id', TEXT_DOMAIN),
            'param_name' => 'popup_id',
            'value' => $contact_forms,
            'save_always' => true,
            'description' => __('Choose contact form list.', TEXT_DOMAIN),
            'dependency' => array(
                'element' => 'button_target',
                'value' => '3',
            ),
        ),
        array(
                "type" => "vc_link",
                "holder" => "div",
                "heading" => "Action Button",
                "param_name" => "link",
                 'dependency' => array(
                    'element' => 'button_target',
                    'value' => '4',
                        ),  
             ),
    )
));