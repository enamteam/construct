<?php
vc_map(array(
    "name" => "Slick Slider",
    "base" => "Cleanhome_Additional_owlslider",
    "category" => 'Theme Additional',
    "as_parent" => array('only' => 'Cleanhome_Additional_owlslider_content'),
    "content_element" => true,
    
    "show_settings_on_create" => true,
    "js_view" => 'VcColumnView',
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => "Extra Class",
            "param_name" => "extra_class",
        ),
    )
));

vc_map(array(
    "name" => "Slick Slider Items",
    "base" => "Cleanhome_Additional_owlslider_content",
    "category" => 'cryptocoin',
    "as_child" => array('only' => 'Cleanhome_Additional_owlslider'),
    "content_element" => true,
    
    "show_settings_on_create" => true,
    "params" => array(
        array(
            "type" => "attach_image",
            "heading" => esc_html__("Slider Image", TEXT_DOMAIN),
            "param_name" => "image",
        ),
       
        array(
            "type" => "textfield",
            "heading" => "Title 1st Line",
            "param_name" => "title1",
            "admin_label" => false,
        ),
		array(
            "type" => "textfield",
            "heading" => "Title 2nd Line",
            "param_name" => "title2",
            "admin_label" => false,
        ),
		array(
            "type" => "textfield",
            "heading" => "Subtitle",
            "param_name" => "subtitle",
            "admin_label" => false,
        ),
		
		array(
                "type" => "vc_link",
                "holder" => "div",
                "heading" => "Button One Link",
                "param_name" => "button_1_link",
        ),
		array(
                "type" => "vc_link",
                "holder" => "div",
                "heading" => "Button Two Link",
                "param_name" => "button_2_link",
             ),
		 array(
            "type" => "textfield",
            "heading" => "Add Extra Class",
            "param_name" => "extraclass",
        ),
    )
));

if (class_exists('WPBakeryShortCodesContainer')) {

    class WPBakeryShortCode_Cleanhome_Additional_Owlslider extends WPBakeryShortCodesContainer {
        
    }

}
if (class_exists('WPBakeryShortCode')) {

    class WPBakeryShortCode_Cleanhome_Additional_Owlslider_Content extends WPBakeryShortCode {
        
    }

}

