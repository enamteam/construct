<?php
vc_map(array(
    "name" => "Welcome",
    "base" => "Cleanhome_Additional_welcome",
    "category" => 'Theme Additional',
    "as_parent" => array('only' => 'Cleanhome_Additional_welcome_content'),
    "content_element" => true,
    
    "show_settings_on_create" => true,
    "js_view" => 'VcColumnView',
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => "Extra Class",
            "param_name" => "extra_class",
        ),
    )
));

vc_map(array(
    "name" => "Welcome Items",
    "base" => "Cleanhome_Additional_welcome_content",
    "category" => 'cryptocoin',
    "as_child" => array('only' => 'Cleanhome_Additional_welcome'),
    "content_element" => true,
    
    "show_settings_on_create" => true,
    "params" => array(
        
        array(
            "type" => "textfield",
            "heading" => __( "Number", TEXT_DOMAIN) ,
            "param_name" => "number",
            "admin_label" => false,
        ),
        array(
        'type' => 'iconpicker',
        'heading' => __( 'Icon', 'js_composer' ),
        'param_name' => 'icon_fontawesome',
        'settings' => array(
             'emptyIcon' => false, 
            'iconsPerPage' => 200, // default 100, how many icons

        ),
        'dependency' => array(
            'element' => 'type',
           'value' => 'fontawesome',
         ),
        ),
       
        array(
            "type" => "textfield",
            "heading" => __( "Title", TEXT_DOMAIN) ,
            "param_name" => "title",
            "admin_label" => false,
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Description", TEXT_DOMAIN),
            "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
            "value" => __( "", TEXT_DOMAIN ),
            "description" => __( "Enter your Description.", "my-text-domain" )
         ),
		
    )
));

if (class_exists('WPBakeryShortCodesContainer')) {

    class WPBakeryShortCode_Cleanhome_Additional_Welcome extends WPBakeryShortCodesContainer {
        
    }

}

if (class_exists('WPBakeryShortCode')) {

    class WPBakeryShortCode_Cleanhome_Additional_welcome_content extends WPBakeryShortCode {
        
    }

}
