<?php
function cleanhome_print_css($props = '', $values = array(), $vkey = '', $pre_fix = '', $post_fix = '')
{
    if (isset($values[$vkey]) && !empty($values[$vkey])) {
        print wp_kses_post($props.":".$pre_fix.$values[$vkey].$post_fix.";\n");
    }
    
}
function cleanhome_inline_style(){
global $clean_home_options;
 ob_start();
echo '.page-area {
    background: rgba(0, 0, 0, 0) url("'.$clean_home_options['clean_home_breadcrumb_brackground']['url'].'") no-repeat scroll center top / cover ;
}';
echo 'div#preloader {
    	background: '.$clean_home_options['clean_home_color_preloder_background']['color'].' url("'.$clean_home_options['clean_home_preloader_brackground']['url'].'") no-repeat center center;
	}';


    $cryptocoin_inline_style_css = ob_get_clean();
    return $cryptocoin_inline_style_css;
}