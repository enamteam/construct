<?php

class cleanhome_about_us extends WP_Widget {
   public $defaults;
   public function __construct() {
        $this->defaults = array(
            'title' => esc_html__('Contacts', 'clean-home'),
			'aboutus' => esc_html__('Redug Lagre dolor sit amet, consectetur adipisicing elit. Minima in nostrum, veniam. Esse est assumenda inventore.adipisicing elit. Minima in nostrum, veniam. Esse est assumenda inventore.', 'clean-home'),
            'facebook' => '#',            
            'twitter' => '#',            
            'googleplus' => '#',            
            'pinterest' => '#',            
            'vimeo' => '#',            
            'flickr' => '#',            
            'linkedin' => '#',            
            'youtube' => '#',            
            'dribbble' => '#',            
            'instagram' => '#'  
        );
        parent::__construct(
                'cleanhome_about_us_widget', // Base ID  
                esc_html__('Cleanhome About Us', 'clean-home'), // Name  
                array(
            'description' => esc_html__('This widget will display About Us.', 'clean-home')
                )
        );
   }

    function form($instance) {
        $instance = wp_parse_args((array) $instance, $this->defaults);
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <strong><?php esc_html_e('Title', 'clean-home') ?>:</strong><br /><input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />

            </label>
        </p> 
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('aboutus')); ?>"><?php esc_html_e('About Us Details', 'clean-home') ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('aboutus')); ?>" name="<?php echo esc_attr($this->get_field_name('aboutus')); ?>"><?php echo wp_kses_post($instance['aboutus']); ?></textarea>
        </p>
        <!-- facebook-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('facebook')); ?>"><?php esc_html_e('Facebook', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('facebook')); ?>" name="<?php echo esc_attr($this->get_field_name('facebook')); ?>"><?php echo wp_kses_post($instance['facebook']); ?></textarea>
        </p>
        <!-- twiiter-->
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('twitter')); ?>"><?php esc_html_e('Twitter', 'clean-home') ?></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('twitter')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter')); ?>"><?php echo wp_kses_post($instance['twitter']); ?></textarea>
        </p>
        <!-- googleplus-->
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('googleplus')); ?>"><?php esc_html_e('Googleplus', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('googleplus')); ?>" name="<?php echo esc_attr($this->get_field_name('googleplus')); ?>"><?php echo wp_kses_post($instance['googleplus']); ?></textarea>

        </p>
        <!-- pinterest-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('pinterest')); ?>"><?php esc_html_e('Pinterest', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('pinterest')); ?>" name="<?php echo esc_attr($this->get_field_name('pinterest')); ?>"><?php echo wp_kses_post($instance['pinterest']); ?></textarea>

        </p>
        <!-- vimeo-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('vimeo')); ?>"><?php esc_html_e('Vimeo', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('vimeo')); ?>" name="<?php echo esc_attr($this->get_field_name('vimeo')); ?>"><?php echo wp_kses_post($instance['vimeo']); ?></textarea>

        </p>
        <!-- flickr-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('flickr')); ?>"><?php esc_html_e('Flickr', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('flickr')); ?>" name="<?php echo esc_attr($this->get_field_name('flickr')); ?>"><?php echo wp_kses_post($instance['flickr']); ?></textarea>

        </p>
        <!-- linkedin-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('linkedin')); ?>"><?php esc_html_e('Linkedin', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('linkedin')); ?>" name="<?php echo esc_attr($this->get_field_name('linkedin')); ?>"><?php echo wp_kses_post($instance['linkedin']); ?></textarea>

        </p>
        <!-- youtube-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('youtube')); ?>"><?php esc_html_e('Youtube', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('youtube')); ?>" name="<?php echo esc_attr($this->get_field_name('youtube')); ?>"><?php echo wp_kses_post($instance['youtube']); ?></textarea>

        </p>
        <!-- dribbble-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('dribbble')); ?>"><?php esc_html_e('Dribbble', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('dribbble')); ?>" name="<?php echo esc_attr($this->get_field_name('dribbble')); ?>"><?php echo wp_kses_post($instance['dribbble']); ?></textarea>

        </p>
        <!-- instagram-->
        <p>

            <label for="<?php echo esc_attr($this->get_field_id('instagram')); ?>"><?php esc_html_e('Instagram', 'clean-home') ?></label>

            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('instagram')); ?>" name="<?php echo esc_attr($this->get_field_name('instagram')); ?>"><?php echo wp_kses_post($instance['instagram']); ?></textarea>

        </p>
        <?php
    }

    function widget($args, $instance) {
        extract($args);
        echo wp_kses_post($before_widget);
        if (!empty($instance['title'])) {
            $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
            echo wp_kses_post($before_title . $title . $after_title);
        };
		
		if (!empty($instance['aboutus'])){
			echo '<p>'.wp_kses_post($instance['aboutus']).'</p>'; 
		}
		
        ?>

        
        <div class="footer-icons">
        <ul class="about-us-social">
           <!--facebook-->              
            <?php
            if (!empty($instance['facebook'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['facebook']); ?>">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
            <?php endif; ?>

        <!--twitter-->

            <?php
            if (!empty($instance['twitter'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['twitter']); ?>">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
            <?php endif; ?>
        
        <!--googleplus-->

            <?php
            if (!empty($instance['googleplus'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['googleplus']); ?>">
                        <i class="fa fa-google"></i>
                    </a>
                </li>
            <?php endif; ?>  

            <!--pinterest-->

            <?php
            if (!empty($instance['pinterest'])):?>
               <li>
                    <a href="<?php echo wp_kses_post($instance['pinterest']); ?>">
                        <i class="fa fa-pinterest"></i>
                    </a>
                </li>
            <?php endif; ?>  

            <!--vimeo-->

            <?php
            if (!empty($instance['vimeo'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['vimeo']); ?>">
                        <i class="fa fa-vimeo"></i>
                    </a>
                </li>
            <?php endif; ?>  

            <!--flickr-->

            <?php
            if (!empty($instance['flickr'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['flickr']); ?>">
                        <i class="fa fa-flickr"></i>
                    </a>
                </li>
            <?php endif; ?>

            <!--linkedin-->

            <?php
            if (!empty($instance['linkedin'])):?>
               <li>
                    <a href="<?php echo wp_kses_post($instance['linkedin']); ?>">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </li>
            <?php endif; ?> 

           <!--youtube-->

            <?php
            if (!empty($instance['youtube'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['youtube']); ?>">
                        <i class="fa fa-youtube"></i>
                    </a>
                </li>
            <?php endif; ?>

            <!--dribbble-->

            <?php

            if (!empty($instance['dribbble'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['dribbble']); ?>">
                        <i class="fa fa-dribbble"></i>
                    </a>
                </li>
            <?php endif; ?>  
            
        <!--instagram-->

            <?php
            if (!empty($instance['instagram'])):?>
                <li>
                    <a href="<?php echo wp_kses_post($instance['instagram']); ?>">
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
            <?php endif; ?>  

        </ul>
        </div>
        <?php
        echo wp_kses_post($after_widget);
    }

    function update($new_instance, $old_instance) {

        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['aboutus'] = $new_instance['aboutus'];
        $instance['facebook'] = $new_instance['facebook'];
        $instance['twitter'] = $new_instance['twitter'];
        $instance['googleplus'] = $new_instance['googleplus'];
        $instance['pinterest'] = $new_instance['pinterest'];
        $instance['vimeo'] = $new_instance['vimeo'];
        $instance['flickr'] = $new_instance['flickr'];
        $instance['linkedin'] = $new_instance['linkedin'];
        $instance['youtube'] = $new_instance['youtube'];
        $instance['dribbble'] = $new_instance['dribbble'];
        $instance['instagram'] = $new_instance['instagram'];

        return $instance;
    }

}
function cleanhome_about_us_call() {
    register_widget( 'cleanhome_about_us' );
}
add_action( 'widgets_init', 'cleanhome_about_us_call' );

