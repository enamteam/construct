<?php

class cleanhome_info extends WP_Widget {
   public $defaults;
   public function __construct() {
        $this->defaults = array(
            'title' => esc_html__('Information', 'clean-home'),
			'subtitle' => esc_html__('You can contact us our consectetur adipisicing elit.', 'clean-home'),
            'tel_title' => esc_html__('Tel:', 'clean-home'),           
            'tel_details' => esc_html__('+013 654 432', 'clean-home'),           
            'email_title' => esc_html__('Email:', 'clean-home'),          
            'email_details' => esc_html__('info@cleanservice.com', 'clean-home'),            
            'location_title' => esc_html__('Location:', 'clean-home'),           
            'location_details' => esc_html__('22 avanue,Newyork', 'clean-home'),              
        );
        parent::__construct(
                'cleanhome_info_widget', // Base ID  
                esc_html__('Cleanhome Iformation', 'clean-home'), // Name  
                array(
            'description' => esc_html__('This widget will display Information.', 'clean-home')
                )
        );
   }

    function form($instance) {
        $instance = wp_parse_args((array) $instance, $this->defaults);
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <strong><?php esc_html_e('Title', 'clean-home') ?>:</strong><br /><input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />

            </label>
        </p> 
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"> <strong><?php esc_html_e('Subtitle', 'clean-home') ?>:</strong><br /></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>"><?php echo wp_kses_post($instance['subtitle']); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('tel_title')); ?>">
                <strong><?php esc_html_e('Field 1 Title', 'clean-home') ?>:</strong><br /><input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('tel_title')); ?>" name="<?php echo esc_attr($this->get_field_name('tel_title')); ?>" value="<?php echo esc_attr($instance['tel_title']); ?>" />

            </label>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('tel_details')); ?>"> <strong><?php esc_html_e('Field 1 Details', 'clean-home') ?>:</strong><br /></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('tel_details')); ?>" name="<?php echo esc_attr($this->get_field_name('tel_details')); ?>"><?php echo wp_kses_post($instance['tel_details']); ?></textarea>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email_title')); ?>">
                <strong><?php esc_html_e('Field 2 Title', 'clean-home') ?>:</strong><br /><input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('email_title')); ?>" name="<?php echo esc_attr($this->get_field_name('email_title')); ?>" value="<?php echo esc_attr($instance['email_title']); ?>" />

            </label>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email_details')); ?>"> <strong><?php esc_html_e('Field 2 Details', 'clean-home') ?>:</strong><br /></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('email_details')); ?>" name="<?php echo esc_attr($this->get_field_name('email_details')); ?>"><?php echo wp_kses_post($instance['email_details']); ?></textarea>
        </p>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('location_title')); ?>">
                <strong><?php esc_html_e('Field 3 Title', 'clean-home') ?>:</strong><br /><input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('location_title')); ?>" name="<?php echo esc_attr($this->get_field_name('location_title')); ?>" value="<?php echo esc_attr($instance['location_title']); ?>" />

            </label>
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('location_details')); ?>"> <strong><?php esc_html_e('Field 3 Details', 'clean-home') ?>:</strong><br /></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('location_details')); ?>" name="<?php echo esc_attr($this->get_field_name('location_details')); ?>"><?php echo wp_kses_post($instance['location_details']); ?></textarea>
        </p>
       
        <?php
    }

    function widget($args, $instance) {
        extract($args);
        echo wp_kses_post($before_widget);
        if (!empty($instance['title'])) {
            $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
            echo wp_kses_post($before_title . $title . $after_title);
        };
		
		if (!empty($instance['subtitle'])){
			echo '<p>'.wp_kses_post($instance['subtitle']).'</p>'; 
		}     
        ?>
        <div class="footer-contacts">
            <p><span><?php echo wp_kses_post($instance['tel_title']) ?></span> <?php echo wp_kses_post($instance['tel_details']) ?></p>
            <p><span><?php echo wp_kses_post($instance['email_title']) ?></span> <?php echo wp_kses_post($instance['email_details']) ?></p>
            <p><span><?php echo wp_kses_post($instance['location_title']) ?></span> <?php echo wp_kses_post($instance['location_details']) ?></p>
        </div>
        <?php
        echo wp_kses_post($after_widget);
    }

    function update($new_instance, $old_instance) {

        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['subtitle'] = $new_instance['subtitle'];
        $instance['tel_title'] = $new_instance['tel_title'];
        $instance['tel_details'] = $new_instance['tel_details'];
        $instance['email_title'] = $new_instance['email_title'];
        $instance['email_details'] = $new_instance['email_details'];
        $instance['location_title'] = $new_instance['location_title'];
        $instance['location_details'] = $new_instance['location_details'];
        return $instance;
    }

}
function cleanhome_info_call() {
    register_widget( 'cleanhome_info' );
}
add_action( 'widgets_init', 'cleanhome_info_call' );