<?php

class cleanhome_newsletter extends WP_Widget {
   public $defaults;
   public function __construct() {
        $this->defaults = array(
            'title' => esc_html__('Subscribe', 'clean-home'),
			'subtitle' => esc_html__('You can contact us our consectetur adipisicing elit.', 'clean-home'),
            'contact_form' =>'',           
                    
        );
        parent::__construct(
                'cleanhome_newsletter_widget', // Base ID  
                esc_html__('Cleanhome Newsletter', 'clean-home'), // Name  
                array(
            'description' => esc_html__('This widget will display Information.', 'clean-home')
                )
        );
   }

    function form($instance) {
        $instance = wp_parse_args((array) $instance, $this->defaults);
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <strong><?php esc_html_e('Title', 'clean-home') ?>:</strong><br /><input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />

            </label>
        </p> 
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"> <strong><?php esc_html_e('Subtitle', 'clean-home') ?>:</strong><br /></label>
            <textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>"><?php echo wp_kses_post($instance['subtitle']); ?></textarea>
        </p>
        
         <p>
            <label for="<?php echo esc_attr($this->get_field_id('contact_form')); ?>"> <strong><?php esc_html_e('Contact Form', 'clean-home') ?>:</strong><br /></label>
            <select id="<?php echo esc_attr($this->get_field_id('contact_form')); ?>" name="<?php echo esc_attr($this->get_field_name('contact_form')); ?>">
            <?php
				$cf7 = get_posts('post_type="wpcf7_contact_form"&numberposts=-1');
				$contact_forms = array();
				if ($cf7) {
					foreach ($cf7 as $cform) {
						echo '<option value="'.$cform->ID.'">'.$cform->post_title.'</option>';
					}
				} else {
					echo esc_html__('No contact forms found', 'js_composer');
				}
			?>
            </select>
        </p>
        
       
        <?php
    }

    function widget($args, $instance) {
        extract($args);
        echo wp_kses_post($before_widget);
        if (!empty($instance['title'])) {
            $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
            echo wp_kses_post($before_title . $title . $after_title);
        };
		
		if (!empty($instance['subtitle'])){
			echo '<p>'.wp_kses_post($instance['subtitle']).'</p>'; 
		}     
        ?>
        <div class="subs-feilds">
            <div class="suscribe-input">
                <?php echo do_shortcode('[contact-form-7 id="' . $instance['contact_form'] . '"]'); ?>
            </div>
        </div>
        <?php
        echo wp_kses_post($after_widget);
    }

    function update($new_instance, $old_instance) {

        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['subtitle'] = $new_instance['subtitle'];
        $instance['contact_form'] = $new_instance['contact_form'];
        return $instance;
    }

}
function cleanhome_newsletter_call() {
    register_widget( 'cleanhome_newsletter' );
}
add_action( 'widgets_init', 'cleanhome_newsletter_call' );
