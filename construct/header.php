<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<?php wp_head();?>
</head>
<body <?php body_class(); ?> >
<?php global $clean_home_options; ?>
<?php if(isset($clean_home_options['clean_home_is_preloader']) && $clean_home_options['clean_home_is_preloader']){ ?>
<div id="preloader"></div>
<?php } ?>
<header>
<?php if(isset($clean_home_options['clean_home_header_top_switch']) && $clean_home_options['clean_home_header_top_switch']){ ?>
            <!-- Start top bar -->
            <div class="topbar-area fix hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class=" col-md-6 col-sm-6">
                            <div class="topbar-left">
                                <p><?php echo $clean_home_options['clean_home_header_top_text'] ?></p> 
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="topbar-right">
                                <ul>
                                    <?php
									if(isset($clean_home_options['clean_home_skype_link']) && $clean_home_options['clean_home_skype_link']!='') {?>
										<li><a href="<?php echo esc_url($clean_home_options['clean_home_skype_link']); ?>"><i class="fa fa-skype"></i></a></li>
								   <?php } ?>
								   <?php
									if(isset($clean_home_options['clean_home_pin_link']) && $clean_home_options['clean_home_pin_link']!='') {?>
										<li><a href="<?php echo esc_url($clean_home_options['clean_home_pin_link']); ?>"><i class="fa fa-pinterest"></i></a></li>
								   <?php } ?>
								   <?php
									if(isset($clean_home_options['clean_home_gp_link']) && $clean_home_options['clean_home_gp_link']!='') {?>
										<li><a href="<?php echo esc_url($clean_home_options['clean_home_gp_link']); ?>"><i class="fa fa-google"></i></a></li>
								   <?php } ?>
								   <?php
									if(isset($clean_home_options['clean_home_pin_link']) && $clean_home_options['clean_home_pin_link']!='') {?>
										<li><a href="<?php echo esc_url($clean_home_options['clean_home_pin_link']); ?>"><i class="fa fa-twitter"></i></a></li>
								   <?php } ?>
								   <?php
									if(isset($clean_home_options['clean_home_fb_link']) && $clean_home_options['clean_home_fb_link']!='') {?>
										<li><a href="<?php echo esc_url($clean_home_options['clean_home_fb_link']); ?>"><i class="fa fa-facebook"></i></a></li>
								   <?php } ?>
                                </ul> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End top bar -->
            <?php } ?>
            <!-- header-area start -->
            <div class="header-middle-area hidden-xs">
                <div class="container">
                    <div class="row">
                        <!-- logo start -->
                        <div class="col-md-4 col-sm-3">
                            <div class="logo">
                                <!-- Brand -->
                                 <?php if ( get_header_image() != NULL ){
									 $image_url=get_header_image();
								 }else{
									 $image_url=$clean_home_options['clean_home_logo']['url'];
								 }
								 ?>
                               <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand page-scroll"> <img src="<?php echo esc_url( $image_url); ?>" alt="<?php echo esc_attr(get_bloginfo( 'title' ) ); ?>" /> </a>
                            
                            </div>
                        </div>
                        <!-- logo end -->
                        <div class="col-md-8 col-sm-9">
                            <div class="header-middle-link">
                            
                            <?php if(isset($clean_home_options['clean_home_header_phone_switch']) && $clean_home_options['clean_home_header_phone_switch']){ ?>
                                <div class="header-info">
                                    <div class="header-icon">
                                        <i class="<?php echo esc_attr($clean_home_options['clean_home_header_phone_icon'] ) ?>"></i>
                                    </div>
                                    <div class="header-info-text">
                                        <span class="info-first"><?php echo $clean_home_options['clean_home_header_phone'] ?></span>
                                        <span class="info-simple"><?php echo $clean_home_options['clean_home_header_phone_second'] ?></span>
                                    </div>
                                </div>
							<?php } ?>
                            <?php if(isset($clean_home_options['clean_home_header_mail_switch']) && $clean_home_options['clean_home_header_mail_switch']){ ?>    
                                <div class="header-info">
                                    <div class="header-icon">
                                        <i class="<?php echo esc_attr($clean_home_options['clean_home_header_mail_icon'] ) ?>"></i>
                                    </div>
                                    <div class="header-info-text">
                                        <span class="info-first"><?php echo $clean_home_options['clean_home_header_mail'] ?></span>
                                        <span class="info-simple"><?php echo $clean_home_options['clean_home_header_mail_second'] ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if(isset($clean_home_options['clean_home_header_button_switch']) && $clean_home_options['clean_home_header_button_switch']){ ?>
                                
                                <div class="header-info">
                                    <div class="quote-button">
                                        <a href="#" class="quote-btn"  title="Quick view" data-toggle="modal" data-target="#quoteModal">
                                        <?php echo $clean_home_options['clean_home_header_button_title'] ?>
                                        </a>
                                    </div>
                                </div>  
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End middle bar -->
            <div id="sticker" class="header-area hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- mainmenu start -->
                            <nav class="navbar navbar-default">
                                <div class="collapse navbar-collapse" id="navbar-example">
                                    <div class="main-menu">
                                        <?php
									wp_nav_menu(
									  array(
										'theme_location' => 'primary',
										'container' => 'ul',
    									'menu_class'=> 'nav navbar-nav navbar-right'
									  )
									);
									?>
                                    </div>
                                </div>
                            </nav>
                            <!-- mainmenu end -->
                            <div class="header-right-link">
                               
                               <?php get_template_part( 'custom', 'searchform' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-area end -->
            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area hidden-lg hidden-md hidden-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <div class="logo">
                                     <?php if ( get_header_image() != NULL ){
									 $image_url=get_header_image();
								 }else{
									 $image_url=$clean_home_options['clean_home_logo']['url'];
								 }
								 ?>
                               <a href="<?php echo esc_url( home_url( '/' ) ); ?>"> <img src="<?php echo esc_url( $image_url); ?>" alt="<?php echo esc_attr(get_bloginfo( 'title' ) ); ?>" /> </a>
                                </div>
                                <nav id="dropdown">
                                    <?php
									wp_nav_menu(
									  array(
										'theme_location' => 'primary',
										'container' => 'ul',
    									'menu_class'=> 'nav navbar-nav navbar-right'
									  )
									);
									?>

                                </nav>
                            </div>					
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile-menu-area end -->		
        </header>

<?php echo construct_get_breadcrumb() ?>
