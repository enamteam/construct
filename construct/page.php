<?php get_header(); ?>

	<div id="pageContent" class="content-area">
    <div class="block">
        <div id="primary" class="container">

			<?php 
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					get_template_part( 'template/content', get_post_type() );
				endwhile; endif; 
			?>
</div>
		</div> <!-- /.col -->
	</div> <!-- /.row -->

<?php get_footer(); ?>